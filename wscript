#! /usr/bin/env python
# vim: set fileencoding=utf-8 ts=4 sw=4 et

def configure(conf):
    f = conf.env.append_value
    f('INCLUDES',  [ conf.path.find_dir('src').abspath() ])

def build(bld):
    bld.ecpp_build(
        target   = 'ecpp-job',
        source   = [
            'src/ecpp/Execution/Job.cpp',
            'src/ecpp/Execution/JobTimer.cpp',
        ],
        features = 'cxx cstlib',
    )
