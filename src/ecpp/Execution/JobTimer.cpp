/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#include "ecpp/Execution/LockGuard.hpp"
#include "ecpp/Execution/JobTimer.hpp"
#include "System.hpp"

using namespace ::ecpp::Execution;

void 
TimerQueue::Timer::Stop()
{
  auto & timer_queue = ::sys::GetTimerQueue(*this);
  ::std::remove_reference_t<decltype(timer_queue)>::LockGuard lock(timer_queue);

  timer_queue.RemoveTimer(*this);
  job_        = nullptr;
}

void
TimerQueue::Timer::Start(TimeoutType timeout, Job& job)
{
  auto & timer_queue = ::sys::GetTimerQueue(*this);
  ::std::remove_reference_t<decltype(timer_queue)>::LockGuard lock(timer_queue);

  timer_queue.RemoveTimer(*this);
  job_ = &job;
  ::ecpp::System::DeadlineTimer::Start(timeout);
  timer_queue.AddTimer(*this);
}

void 
TimerQueue::Timer::Continue(TimeoutType timeout, Job& job)
{
  auto & timer_queue = ::sys::GetTimerQueue(*this);
  ::std::remove_reference_t<decltype(timer_queue)>::LockGuard lock(timer_queue);

  timer_queue.RemoveTimer(*this);
  job_ = &job;
  ::ecpp::System::DeadlineTimer::Continue(timeout);

  timer_queue.AddTimer(*this);
}

void
TimerQueue::AddTimer(Timer& timer)
{
  Timer** it = &pending_timers_;

  while(*it != nullptr)
  {
    if (timer.deadline().IsBefore((*it)->deadline()))
      break;

    it = &( (*it)->next_timer_);
  }

  timer.next_timer_ = *it;
  *it = &timer;
}

void
TimerQueue::RemoveTimer(Timer& timer)
{
  Timer** it = &pending_timers_;

  while(*it != nullptr)
  {
    if (*it == &timer)
    {
      *it = timer.next_timer_;
      break;
    }

    it = &( (*it)->next_timer_);
  }

  timer.next_timer_ = nullptr;
}



