/*
 *  Copyright 2019,2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_EXECUTION_JOBTIMER_HPP_
#define ECPP_EXECUTION_JOBTIMER_HPP_

#include "ecpp/Execution/ContextManager.hpp"
#include "ecpp/Execution/Job.hpp"
#include "ecpp/Units/Time.hpp"

#include "ecpp/System/TickTimer.hpp"

#include <cstdint>
#include <type_traits>

namespace ecpp::Execution
{
  class TimerQueue
  {
  public:
    using DeadlineType = ::ecpp::System::Tick;
    using TimeoutType  = ::ecpp::System::Timeout;

    class Timer;
  
  protected:
    template<typename LockOwner>
    Job*      CheckNextTimeout(LockOwner& owner, DeadlineType &next_timeout);

  private:
    void      AddTimer(Timer& timer);
    void      RemoveTimer(Timer& timer);

    Timer    *pending_timers_ {nullptr};
  };

  class TimerQueue::Timer : protected ecpp::System::DeadlineTimer
  {
  public:
    using ecpp::System::DeadlineTimer::IsExpired;
    
    void  Continue(TimeoutType ticks, Job &job);

    template<typename _TimeoutValue, typename _Scale>
    void  Continue(::ecpp::Units::TimeSpan<_TimeoutValue, _Scale> timeout, Job &job)
    {
      Continue(TimeoutType(timeout.CeilTo(TimeoutType::ScaleType())), job);
    }

    void  Start   (TimeoutType ticks, Job &job);

    template<typename _TimeoutValue, typename _Scale>
    void  Start(::ecpp::Units::TimeSpan<_TimeoutValue, _Scale> timeout, Job &job)
    {
      Start(TimeoutType(timeout.CeilTo(TimeoutType::ScaleType())), job);
    }

    void  Stop();

    constexpr bool running() const { return job_ != nullptr; }

  private:
    Job*                  job_         {nullptr};
    Timer*                next_timer_  {nullptr};

    friend class TimerQueue;
  };

  template<typename LockOwner>
  Job* TimerQueue::CheckNextTimeout(LockOwner &owner, DeadlineType &next_timeout)
  { 
    typename ::std::remove_reference_t<decltype(owner)>::LockGuard lock(owner);
    Timer* next_timer = pending_timers_;

    if(next_timer != nullptr)
    {
      auto current_ticks = ecpp::System::GetTick();

      if(next_timer->IsExpired(current_ticks))
        RemoveTimer(*next_timer);
      else
        next_timer = nullptr;

      if(pending_timers_ != nullptr)
        next_timeout = pending_timers_->deadline();
    }

    if(next_timer != nullptr)
    {
      Job* job = next_timer->job_;
      next_timer->job_ = nullptr;
      return job;
    }
    else
    {
      return nullptr;
    }
  }


  #define ECPP_JOB_TIMER_DECL(name) \
    void name ## Handler (void); \
    struct name ## Class { \
      public:  void Enqueue(); \
      public:  template<typename T> void Start(T t) { job_.SetHandler(Exec); timer_.Start(t, job_); } \
      public:  template<typename T> void Continue(T t)  { job_.SetHandler(Exec); timer_.Continue(t, job_); } \
      public:  void Stop()                          { timer_.Stop(); } \
      public:  bool IsRunning() const               { return timer_.running(); } \
      private: static void Exec(::ecpp::Execution::Job &job); \
      private: ::ecpp::Execution::Job   job_; \
      private: ::ecpp::Execution::TimerQueue::Timer timer_; \
    } name 

}


#endif /* ECPP_EXECUTION_JOBQUEUE_HPP_ */
