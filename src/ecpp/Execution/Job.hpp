/*
 *  Copyright 2019,2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_EXECUTION_JOB_HPP_
#define ECPP_EXECUTION_JOB_HPP_

#include "ecpp/Execution/LockGuard.hpp"
#include <cstdint>

namespace ecpp::Execution
{
  /** Implementation of a simple function pointer based job queue
   * mechanism. Intended to be used for serializing and asynchronous processing
   * of tasks of limited duration. */
  class JobQueue;

  /** Used to queue an action to be performed */
  class Job
  {
  public:
    typedef void (*Handler)(Job &);

    constexpr Job() {};
    constexpr Job(Handler handler) : handler_{handler} {};

    void SetHandler(Handler handler)
    {
      handler_ = handler;
    }

    void run()
    {
      if (nullptr == handler_)
        return;

      handler_(*this);
    }

  private:
    Job     *next_job_ {nullptr};
    Handler  handler_  {nullptr};

    friend class JobQueue;
  };

  class JobQueue
  {
  private:
    template<typename Owner>
    using LockGuard = ::ecpp::Execution::LockGuard<typename Owner::Lockable>;
  protected:

    template<typename Owner>
    void Enqueue(Owner& owner, Job &job)
    {
      LockGuard<Owner> lock(owner);
      PushBack(job);
    }

    template<typename Owner>
    Job* GetNext(Owner& owner)
    {
      LockGuard<Owner> lock(owner);
      return PopFront();
    }
  private:
    void    PushBack(Job &job);
    Job*    PopFront();

    Job*    head_;
    Job*    tail_;
  };

  #define ECPP_JOB_DECL(name) \
    void name ## Handler (void); \
    struct name ## Class { \
      public:  void Enqueue(); \
      private: static void Exec(::ecpp::Execution::Job &job);\
      private: ::ecpp::Execution::Job job_; \
    } name 

  #define ECPP_JOB_DEF(type, name)\
    void type::name ## Class::Enqueue()             { ::sys::Enqueue( container_of(type, name, *this), job_,  Exec); }\
    void type::name ## Class::Exec(::ecpp::Execution::Job &job) { container_of(type, name, container_of(type::name ## Class, job_, job) ).name ## Handler(); } \
    void type::name ## Handler ()

  #define ECPP_TEMPLATE_JOB_DEF(templ,type, name)\
    templ void type::name ## Class::Enqueue()                       { ::sys::Enqueue( container_of(type, name, *this), job_,  Exec); }\
    templ void type::name ## Class::Exec(::ecpp::Execution::Job &job) { container_of(type, name, container_of(type::name ## Class, job_, job) ).name ## Handler(); } \
    templ void type::name ## Handler ()

}

#endif /* ECPP_EXECUTION_JOB_HPP_ */
